# -*- coding: utf-8 -*-
from glob import glob
from re import compile
from collections import Counter, defaultdict
from functools import reduce
from MeCab import Tagger
from math import sqrt
from typing import List, Dict, Set, Sequence, Callable, Iterable, Tuple


class Page(Counter):
    name = str()
    index = defaultdict(set)

    def __init__(self, name: str, index: defaultdict(Set)) -> None:
        super(Page, self).__init__()
        self.name = name
        self.index = index

    def write(self, token: str) -> None:
        self[token] += 1
        self.index[token].add(self.name)

    def write_substrings(self, sequence: Sequence[str], n: int) -> None:
        # 区間をnごとに切り出して辞書に追加
        for i in range(len(sequence) - n + 1):
            token = "".join(sequence[i:i + n])
            self.write(token)

    def dump(self, path: str) -> None:
        with open(path, "w") as f:
            for token in sorted(self):
                f.write("{}\t{}\n".format(token, self[token]))

    def normalize(self, denomination: float) -> None:
        for token in self:
            self[token] /= denomination

    def calc_magnitude(self) -> float:
        def add_sq(base, x):
            return base + x * x

        return sqrt(reduce(add_sq, self.values(), 0))

    def __repr__(self) -> str:
        return "<Page: {}>".format(self.name)


class Book:
    _book = dict()
    index = defaultdict(set)

    def __init__(self, file_names) -> None:
        self.index = defaultdict(set)
        self._book = {file_name: Page(file_name, self.index)
                      for file_name in file_names}
    
    def get_page(self, page_name: str) -> Page:
        return self._book[page_name]

    def multiply_idf(self, idf_dict: Dict[str, Set[str]]) -> None:
        for page in self:
            for token in page:
                page[token] *= idf_dict[token]

    def dump(self, path_specifier: Callable[[Page], str]) -> None:
        for page in self:
            page.dump(path_specifier(page))

    def __iter__(self) -> Iterable[Page]:
        return iter(self._book.values())


class SearchBook(Book):
    def __init__(self) -> None:
        super().__init__(glob("*.txt"))

    @staticmethod
    def load_page(page_name: str) -> Page:
        page = Page(page_name, None)
        for n in range(1, 6):
            for line in open("result-character/{}.{}grams".format(page_name, n)):
                data = line.split('\t')
                page[data[0]] += float(data[1])
        for line in open("result-word/{}.1words".format(page_name)):
            data = line.split('\t')
            page[data[0]] += float(data[1])

        return page

    def get_page(self, page_name: str) -> Page:
        if page_name not in self._book:
            self._book[page_name] = self.load_page(page_name)

        return self._book[page_name]


# mecab.parseの結果から「原型」or「表層」を取り出す。
def get_first(s: str) -> str:
    s = s.split("\t")
    if len(s) < 2:
        return ''
    return s[0] or s[1]

mecab = Tagger("-F%f[6]\\t%m\\n -E\ ")  # "原型\t表層\n..." 形式の文字列を吐き出す解析器


def get_mecab_word_list(line: str) -> List[str]:
    return list(filter(None, [get_first(s) for s in mecab.parse(line).split('\n')]))


def build_tf_books(file_names: List[str]) -> Tuple[List[Book], List[Book]]:
    space_re = compile(r'\s*')

    gram_books, word_books = [i and Book(file_names) for i in range(6)],\
                             [i and Book(file_names) for i in range(4)]

    for file_name in file_names:
        lines = [space_re.sub('', line) for line in open(file_name)]
        gram_count, word_count = [0] * 6, [0] * 4

        gram_pages = [book and book.get_page(file_name) for book in gram_books]
        word_pages = [book and book.get_page(file_name) for book in word_books]

        for line in lines:
            for n in range(1, 6):
                gram_count[n] += max(0, len(line) - n + 1)
                gram_pages[n].write_substrings(line, n)

            # mecab解析の結果から原型、なければ表層を取り出し、不正なトークンは除去した単語リスト
            mecab_word_list = get_mecab_word_list(line)

            for n in range(1, 4):
                word_count[n] += max(0, len(mecab_word_list) - n + 1)
                word_pages[n].write_substrings(mecab_word_list, n)

        # 出現回数 -> TF
        for n in range(1, 6):
            gram_pages[n].normalize(gram_count[n])
        for n in range(1, 4):
            word_pages[n].normalize(word_count[n])

    return gram_books, word_books


def dump_transposed_index(transposed_index: Dict[str, Set[str]],
                          file_name: str = "transposed_index.dat") -> None:
    with open("search-engine/" + file_name, "w") as f:
        for token in transposed_index:
            f.write(token)
            for file_name in sorted(transposed_index[token]):
                f.write("\t" + file_name)
            f.write("\n")
