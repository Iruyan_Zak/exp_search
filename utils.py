# -*- coding: utf-8 -*-

import threading
from os import makedirs
from typing import Callable
import resource
from sys import stderr


class Timer:
    name = str()
    output_path = None
    count = int()
    start_time = float()

    def __init__(self, name: str="print_time", output_file_name: str=None) -> None:
        self.name = name
        self.reset()
        if output_file_name:
            makedirs("stats", exist_ok=True)
            self.output_path = "stats/" + output_file_name

    def reset(self) -> None:
        self.start_time = resource.getrusage(resource.RUSAGE_SELF).ru_utime

    def write(self, msg: str) -> None:
        stderr.write(msg)
        if self.output_path:
            open(self.output_path, "a").write(msg)

    def print_time(self) -> None:
        self.count += 1
        usage = resource.getrusage(resource.RUSAGE_SELF)
        msg = "{}[{}]: {:.3f} seconds has elapsed.\n"\
            .format(self.name, self.count, usage.ru_utime - self.start_time)
        self.write(msg)

    def __enter__(self) -> None:
        self.write("{}: Start.\n".format(self.name))
        self.reset()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        self.print_time()
        self.write("{}: Finish.\n\n".format(self.name))


class MemoryUsageLogger:
    output_path = str()
    start_time = float()
    thread = None

    def __init__(self, output_file_name: str="memory_usage.dat") -> None:
        makedirs("stats", exist_ok=True)
        self.output_path = "stats/" + output_file_name
        self.start_time = resource.getrusage(resource.RUSAGE_SELF).ru_utime

    def print_usage(self) -> None:
        usage = resource.getrusage(resource.RUSAGE_SELF)
        with open(self.output_path, "a") as f:
            f.write("{:.3f}\t{}\n".format(usage.ru_utime - self.start_time, usage.ru_maxrss))

    def callback(self, interval: float, func: Callable[[], None]) -> None:
        func()
        self.thread = threading.Timer(interval, self.callback, (interval, func))
        self.thread.start()

    def __enter__(self) -> None:
        self.callback(3, self.print_usage)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        self.thread.cancel()
        self.print_usage()
