#!/usr/bin/env python
# -*- coding: utf-8 -*-

from functools import reduce
from math import sqrt
from utils import Timer, MemoryUsageLogger
from typing import Set, Iterable, Tuple, List
from models import get_mecab_word_list, Page, SearchBook
from sys import stderr


class SearchEngine:
    transposed_index = dict()
    document_magnitude = dict()
    search_book = None
    search_method, order_method = None, None
    output_path = str()
    timer = None

    def __init__(self, output_file_name: str="search.log") -> None:
        self.transposed_index = dict()
        self.document_magnitude = dict()
        self.search_book = SearchBook()
        self.search_method, self.order_method = self.or_search, self.weight_order
        self.output_path = "stats/" + output_file_name
        self.timer = Timer("search_engine.search", output_file_name)

        with Timer("search_engine.init") as timer, MemoryUsageLogger("memory_usage_search.dat"):
            for line in open("search-engine/transposed_index.dat"):
                data = line.rstrip().split("\t")
                self.transposed_index[data[0]] = set(data[1:])
            timer.print_time()

            for line in open("search-engine/document_magnitude.dat"):
                data = line.rstrip().split('\t')
                self.document_magnitude[data[0]] = float(data[1])

    def interpret_command(self, command: str) -> str:
        if command == "q":
            return "Quit."
        elif command == "or":
            self.search_method = self.or_search
        elif command == "and":
            self.search_method = self.and_search
        elif command == "weight":
            self.order_method = self.weight_order
        elif command == "sim":
            self.order_method = self.sim_order
        else:
            return "Command not found: " + command
        return "Method: " + command + " is successfully set."

    def search(self, search_words: Iterable[str]) -> Tuple[List[Tuple[float, str]], int]:
        with self.timer as timer, open(self.output_path, "a") as f:
            search_words = set(search_words)
            f.write("検索方法: {}\n表示順: {}\n".format(self.search_method.__name__, self.order_method.__name__))
            f.write("検索ワード: {}\n".format(", ".join("{}".format(w) for w in search_words)))
            hit_documents = []
            for word in search_words:
                try:
                    hit_documents.append(self.transposed_index[word])
                except KeyError as _:
                    if self.search_method == self.and_search:
                        return [], 0

            try:
                pages = [self.search_book.get_page(file_name)
                         for file_name in self.search_method(hit_documents)]
            except TypeError as _:
                return [], 0

            search_result, tokens = self.order_method(pages, search_words)
            search_result = list(search_result)
            timer.print_time()

            f.write("\n評価に使用するトークン数: {}\n".format((len(tokens))))
            for token in sorted(tokens):
                f.write(token + '\n')
            f.write("\n検索結果\n")
            for result in search_result:
                f.write("{}\t{:.18f}\n".format(result[1], result[0]))
            return search_result[:10], len(search_result)

    @staticmethod
    def and_search(hit_documents: Iterable[Set[str]]) -> Set[str]:
        return reduce(lambda s1, s2: s1 & s2, hit_documents)

    @staticmethod
    def or_search(hit_documents: Iterable[Set[str]]) -> Set[str]:
        return reduce(lambda s1, s2: s1 | s2, hit_documents)

    def weight_order(self, pages: Iterable[Page], search_words: Iterable[str]) -> Tuple[Iterable[str], Set[str]]:
        return sorted(((sum(page[token] for token in search_words), page.name)
                       for page in pages), reverse=True), set(search_words)

    def sim_order(self, pages: Iterable[Page], search_words: Iterable[str]) -> Tuple[Iterable[str], Set[str]]:
        pages = list(pages)
        tokens = (set(page.keys()) for page in pages)
        tokens = reduce(lambda t1, t2: t1 & t2, tokens)
        base_magnitude = sqrt(len(tokens))
        return sorted(((sum(page[token] for token in tokens) /
                        self.document_magnitude[page.name] / base_magnitude, page.name)
                       for page in pages), reverse=True), tokens


def main() -> None:
    search_engine = SearchEngine()

    while True:
        print("検索ワードを入力してください: ", end="")
        search_words = []
        while not search_words:
            search_words = get_mecab_word_list(input())

        if search_words[0] == ":":
            msg = search_engine.interpret_command(search_words[1])
            stderr.write(msg + '\n')
            if msg == "Quit.":
                return 0
            continue

        search_result, length = search_engine.search(search_words)
        print("検索結果: {}件".format(length))
        for w, p in search_result:
            print("{}: {}".format(p, w))


if __name__ == "__main__":
    try:
        main()
    except (EOFError, KeyboardInterrupt) as _:
        stderr.write("Quit.\n")
