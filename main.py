#!/usr/bin/env python
# -*- coding: utf-8 -*-

from glob import glob
from math import log2
from utils import Timer, MemoryUsageLogger
from os import makedirs
import models


def main() -> None:
    file_names = glob("*.txt")
    file_count = len(file_names)
    makedirs("result-character", exist_ok=True)
    makedirs("result-word", exist_ok=True)
    makedirs("search-engine", exist_ok=True)

    with Timer("main.py", "timer.log") as timer, MemoryUsageLogger("memory_usage_main.dat"):
        gram_books, word_books = models.build_tf_books(file_names)

        timer.print_time()

        transposed_index = word_books[1].index
        for book in gram_books[1:]:
            for token, file_name_set in book.index.items():
                transposed_index[token] |= file_name_set

        timer.print_time()

        models.dump_transposed_index(transposed_index)

        timer.print_time()

        for book in word_books[2:]:
            for token, file_name_set in book.index.items():
                transposed_index[token] |= file_name_set

        idf_dict = {k: log2(file_count/len(v))+1 for k, v in transposed_index.items()}

        timer.print_time()

        # IDF計算済みのモデルの作成とファイル出力
        for n in range(1, 6):
            gram_books[n].multiply_idf(idf_dict)
            gram_books[n].dump(lambda p: "result-character/{}.{}grams".format(p.name, n))

        for n in range(1, 4):
            word_books[n].multiply_idf(idf_dict)
            word_books[n].dump(lambda p: "result-word/{}.{}words".format(p.name, n))

        timer.print_time()

        # 文書の絶対値の計算と出力
        with open("search-engine/document_magnitude.dat", "w") as f:
            for page in sorted(word_books[1], key=lambda p: p.name):
                for gram_book in gram_books[1:]:
                    page.update(gram_book.get_page(page.name))
                f.write("{}\t{}\n".format(page.name, page.calc_magnitude()))


if __name__ == "__main__":
    main()
